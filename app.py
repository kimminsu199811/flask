from flask import Flask, request, jsonify
import json

app = Flask(__name__)

@app.route("/")
def index():
   return 'Hello bob from kimminsu'

@app.route("/add",methods=['GET'])
def add():
   a,b=int(request.args.get('a')),int(request.args.get('b'))
   c=a+b
   return str(c)

@app.route("/sub",methods=['GET'])
def sub():
   a,b=int(request.args.get('a')),int(request.args.get('b'))
   c=a-b
   return str(c)

if __name__ == '__main__':
   app.run(host='0.0.0.0',port=8020)
