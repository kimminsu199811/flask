import unittest
import requests

class UnitTest(unittest.TestCase):
   def setUp(self):
      self.host = 'http://13.209.15.210:8020'
        
   def test_add(self):
      response = requests.get(self.host+'/add?a=1&b=2')
      data = response.text
      self.assertEqual('3', data)

   def test_add2(self):
      response = requests.get(self.host+'/add'+'?a=3&b=5')
      data = response.text
      self.assertEqual('8', data)
      
   def test_sub(self):
      response = requests.get(self.host+'/sub'+'?a=3&b=5')
      data = response.text
      self.assertEqual('-2', data)
      
if __name__ == '__main__':
    unittest.main()
